# Project 2 - How to DEBUG

After doing so much homework, you must be driven mad by DEBUGing before the deadline. Some minor parts in your program stop you to get the full score. So how to debug? You have to understand that this is a problem that every programmer has to overcome.

This project aims to help you practice designing testcases to find those 'minor parts' in programming.

The whole project is based on game 2048. If you are not familiar with this popular game, please refer to [this website](https://play2048.co//).

---
## Files

There are 3 tasks in total.

### Task 1

Files to read:

    useTestcase/main.cpp

Files that should not be modified:

    useTestcase/testcaseGenerator.cpp

### Task 2 & 3

Files to modify:

    designTestcase/test.cpp

Files that should not be modified:

    designTestcase/autograder.hpp
    designTestcase/autograder.o

---
## Task 1 - Learn to use testcase

In this section, you will learn how to use multiple testcases to test your code with one-line command.

You should enter the folder `useTestcase` first.

In file `main.cpp`, you will find a function `isPowerOfTwo`, that is what we are going to test. The input is an integer, and the output should be `true` if the integer is the power of 2 and `false` otherwise.

Try to compile `main.cpp` with

    g++ main.cpp -o main

Then generate the testcases with

    g++ testcaseGenerator.cpp -o testcaseGenerator

and run the executable file `testcaseGenerator.exe` or `testcaseGenerator`. You should see 20 files appear in your folder: 10 `*.in` and 10 `*.out`.

Now you need to find if `isPowerOfTwo` runs as we expected. Just follow the instructions below:

- On Windows, if you like to use `cmd`. Run

```cmd
    for %%i in (*.in) do (main.exe <%%~ni.in >%%~ni.solution)
    fc *.out *.solution
```

- On Windows, if you like to use `PowerShell` (many IDEs likes it!). Run

```powershell
    Get-ChildItem ./*.in | ForEach-Object -Process{ Get-Content $_.name | ./main.exe | Out-File ($_.name -replace '.in', '.solution') }
    Get-ChildItem ./*.out | ForEach-Object -Process{ Write-Host($_.name); Compare-Object (Get-Content $_.name) (Get-Content ($_.name -replace '.out', '.solution')) }
```

- On Linux, run

```Bash
    for file in `ls *.in`; do ./main <$file >${file%.in}.solution; done
    for file in `ls *.out`; do echo $file; diff $file ${file%.out}.solution; done
```

The first line is to generate the solution from `main` to `*.solution`. The second line command is to compare the `.solution` file to the `.out` file. And you can see that `main` does not pass test 2. Open `main.cpp`, you will find that this is because I disabled an important line: if `n` is less than or equal to 0, then put `false`. Remove `//` at that line, compile `main.cpp`, and try again.

This time, just test `2.in`. Follow the instructions below:

- On Windows, if you like to use `cmd`. Run

```cmd
    main.exe <2.in >2.solution
    fc 2.out 2.solution
```

- On Windows, if you like to use `PowerShell` (many IDEs likes it!). Run

```powershell
    Get-Content 2.in | ./main.exe | Out-File 2.solution
    Compare-Object (Get-Content 2.out) (Get-Content 2.solution)
```

- On Linux, run

```Bash
    main <2.in >2.solution
    diff 2.out 2.solution
```

You can see that it successfully pass the test. You don't have to remember the code above, just know that there exists a way to do it, and you can do quicker than others when you want to use a lot of testcases in the future.

Now, it's your turn to design your own testcase.

---
## Task 2 - Build Testcase

In this section, you will design your own testcase for `BasicMove` function in game 2048.

`BasicMove` performs a move in one line. It will merge the tiles to the left, based on the rules of 2048.        

For testing, this function has no return value.

```
EXAMPLE:
  INPUT: line = [2, 4, 4, 0]
 OUTPUT: line = [2, 8, 0, 0]
```

For your convenience, the autograder has been built for you, what you only need to do is add your testcases in `test.cpp`.

There are 10 wrong implements and 1 correct implements in 
total. You have to find out which one is correct with your
testcases, just like the Online Judge.

Only when the correct function passes(`T`) all your testcases and all the wrong function fails(`F`) at least one of your testcases, you can say that you find the correct implementation. 

When you finish building testcase, run

    g++ test.cpp autograder.o -o test 

Then run 'test'(on Linux) or 'test.exe'(on Windows) to check the result. If you find the correct implement, you will receive a message like

    Task 2 PASS - The correct implementation is function *

---
## Task 3  Fewer Testcases

This task is the same as task 1, but the number of testcases is limited to 5!

Finish task 1 within 5 testcases and you will get a surprise!

You can change the template testcase if you want.

When you finish building testcase, run

    g++ test.cpp autograder.o -o test 
 